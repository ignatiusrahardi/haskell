--slide
easy x y z = x*(y+z)

data Day = Sun | Mon | Tue | Wed | Thu | Fri | Sat   deriving Show

valday :: Integer -> Day
valday 1 = Sun
valday 2 = Mon
valday 3 = Tue
valday 4 = Wed
valday 5 = Thu
valday 6 = Fri
valday 7 = Sat

--coba coba
h :: Int -> Int -> Int
h x y = x + y

-- lambda
lambdas l = map (\x -> x + 1) l
-- x:xs
test [] = []
test(x:xs) = (x*2) : test xs

sumList [] = 0
sumList (x:xs) = x + sumList xs

data Direction = N | E | S | W
    deriving (Eq,Show,Enum)

right :: Direction -> Direction
right d = toEnum(succ (fromEnum d) `mod` 4)