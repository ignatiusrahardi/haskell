data Expr = C Float | Expr :+ Expr | Expr :- Expr
         | Expr :* Expr | Expr :/ Expr
         | V [Char]
         | Let String Expr Expr
    deriving Show
