import Data.List
--Latihan HOF
-- 1 (*). Exercises from chapter 9-10 of The Craft of Functional Programming

-- 1 Define the length function using map and sum
len l = sum (map (\x -> 1) l)

-- 2 What does map (+1) (map (+1) xs) do? 
-- Should return +2 of array x
fun x = map (+1) (map (+1) x) 

-- 3 iter 3 f x = f(f(fx))
--iter n f x = 
iter :: Num n => n -> (x -> x) -> x -> x
iter n f x = iter (n-1) f (f x)

-- 5 How would you define the sum of the squares of the natural numbers 1 to n using map and foldr?

sumofsquares1 n = foldr (+) 1 (map (\x -> x^2) [1..n])

-- 6 How does the function below behave?
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- 2 (*). List Comprehensions and Higher-Order Functions

-- Can you rewrite the following list comprehensions using the higher-order
-- functions map and filter? You might need the function concat too.
-- 1. [ x+1 | x <- xs ]
-- 2. [ x+y | x <- xs, y <-ys ]
-- 3. [ x+2 | x <- xs, x > 3 ]
-- 4. [ x+3 | (x,_) <- xys ]
-- 5. [ x+4 | (x,y) <- xys, x+y < 5 ]
-- 6. [ x+5 | Just x <- mxs ]

-- 1 input [1,2,3]
problem1 xs = [ x+1 | x <- xs ]
answer1 xs = map (\x -> x + 1) xs

--2 input [1,2,3] [1,2,3]
problem2 xs ys = [ x+y | x <- xs, y <-ys ]
answer2 xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)

--3 input [1,2,3]
problem3 xs = [ x+2 | x <- xs, x > 3 ]
answer3 xs = map (\x -> x + 2) ( filter (>3) xs) 

--4 
problem4 xys = [ x+3 | (x,_) <- xys ]

-- Can you it the other way around? I.e. rewrite the following expressions as list
-- comprehensions.
-- 1. map (+3) xs
-- 2. filter (>7) xs
-- 3. concat (map (\x -> map (\y -> (x,y)) ys) xs)
-- 4. filter (>3) (map (\(x,y) -> x+y) xys)

-- 1 
problem21 xs = map (+3) xs
answer21 xs= [x+3 | x <- xs]

-- 2
problem22 xs = filter (>7) xs
answer22 xs = [x | x <- xs , x > 7]
