-- Lazy evaluation latihan soal dari scele
import Data.List

-- 1
-- [x+y | x <- [1 .. 4], y <-[2 .. 4], x > y]

-- 2 fungsi divisor yang menerima sebuah bilangan bulat n dan mengembalikan
-- list bilangan bulat positif yang membagi habis n
divisor n = [x | x <- [1 .. n], n `mod` x == 0]

pembagi x n = (n `mod` x == 0)
lebihkecil n = [x | x <- [1 .. n]]

divisor2 n = [x | x <- (lebihkecil n), pembagi x n]

-- 3 quick sort in list comprehension
quickSort [] = []
quickSort(x:xs) = quickSort [y | y <- xs,
                                y <= x]
                            ++ [x] ++
                quickSort [y | y <- xs,
                                y > x]
-- 4 permutation
perm [] = [[]]
perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]
