import Data.List
--1 KPK
kpk x y = take 1 [z | z <- [x..], ( z `mod` x == 0) && (z `mod` y == 0) ] !! 0
fpb a b =  if a < b
    then last [x | x <- [1..a+1], (a `mod` x == 0) && (b `mod` x == 0) ] 
    else last [x | x <- [1..b+1], (a `mod` x == 0) && (b `mod` x == 0) ] 

kpk2 a b =  if a < b
    then last [x | x <- [1..b+1], (a `mod` x == 0) && (b `mod` x == 0) ] 
    else last [x | x <- [1..a+1], (a `mod` x == 0) && (b `mod` x == 0) ] 

--2
test = [(x,y) | x <- [1..3], y <-[1 .. (2*x)] ] 
--4 maxList
-- maxList :: [Int] -> Int
-- maxList [] = 0
-- maxList (x:xs) = foldr max x xs

-- implement max
maxx :: Int -> Int -> Int
maxList :: [Int] -> Int
maxx a b = if a > b
    then a
    else b
maxList xs = foldr (maxx) 0 xs


--3 mergesort
-- merge [] kanan = kanan
-- merge kiri [] = kiri
-- merge kiri@(x:xs) kanan@(y:ys) | x < y = x:merge xs kanan   
--                                 | y <= x = y: merge kiri ys

-- mergesort xs = merge (mergesort kiri) (mergesort kanan)
--     where kiri = take ((length xs)/2) xs    
--           kanan = drop ((length xs)/2) xs

--4 misteri
-- sumPair (x,y) = (x + y)

-- myFunc = map sumPair xs ++ concat xss  
-- myConcat = foldl (myFunc) []

--5